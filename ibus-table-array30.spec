Name:           ibus-table-array30
Version:        1.2.0.20090729
Release:        11
Summary:        The Array 30 Chinese input method for IBus framework
License:        GPL-2.0-or-later
URL:            http://github.com/definite/ibus-table-array30/tree/master
Source0:        http://cloud.github.com/downloads/definite/ibus-table-array30/%{name}-%{version}-Source.tar.gz
Patch0:         ibus-table-array30-1.2.0.20090729-port-to-newer-cmake.patch
BuildArch:      noarch

BuildRequires:  ibus-table >= 1.1 cmake >= 2.4 g++
Requires:       ibus-table >= 1.1

%description
bus-table-array30 provides array 30 Chinese input method on IBus Table under IBus framework.
Use ibus-table input method. This version contains more than 70,000 words in Unicode 3.1.

%prep
%autosetup -n %{name}-%{version}-Source -p1

%build
%cmake
%cmake_build

%install
%cmake_install

%files
%doc AUTHORS README ChangeLog INSTALL
%license COPYING
%{_datadir}/ibus-table/{tables/Array30.db,icons/Array30.png}

%changelog
* Tue Mar 04 2025 Funda Wang <fundawang@yeah.net> - 1.2.0.20090729-11
- try build with cmake 4.0

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 1.2.0.20090729-10
- adopt to new cmake macro

* Tue Feb 06 2024 lvgenggeng <lvgenggeng@uniontech.com> - 1.2.0.20090729-9
- move license file to prefered dir

* Mon Jun 07 2021 wulei <wulei80@huawei.com> - 1.2.0.20090729-8
- fixes failed: No CMAKE_C_COMPILEP could be found

* Wed Dec 4 2019 duyeyu <duyeyu@huawei.com> - 1.2.0.20090729-7
- Package init
